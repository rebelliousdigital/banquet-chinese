<?php
/**
 * Register menus
 */

// This theme uses wp_nav_menu() in two locations.
register_nav_menus( array(
	'left'   => esc_attr__( 'Left header menu', 'banquetchinese' ),
	'right' => esc_attr__( 'Right header menu', 'banquetchinese' ),
) );