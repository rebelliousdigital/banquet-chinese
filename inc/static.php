<?php
/**
 * Include static files: javascript and css
 */

if ( is_admin() ) {
	return;
}

/**
 * Enqueue styles for the front end.
 */

// Add Genericons font, used in the main stylesheet.
wp_enqueue_style(
	'genericons',
	get_template_directory_uri() . '/genericons/genericons.css',
	array(),
	'1.0'
);

if(defined('FW'))
{
	wp_enqueue_style(
		'fw-unycon',
		fw_get_framework_directory_uri( '/static/libs/unycon/unycon.css' ),
		array(),
		''
	);

	wp_enqueue_style(
		'fw-typcn',
		fw_get_framework_directory_uri( '/static/libs/typcn/css/typcn.css' ),
		array(),
		''
	);

	wp_enqueue_style(
		'fw-lnr',
		fw_get_framework_directory_uri( '/static/libs/lnr/css/lnr.css' ),
		array(),
		''
	);

	wp_enqueue_style(
		'fw-linecons',
		fw_get_framework_directory_uri( '/static/libs/linecons/css/linecons.css' ),
		array(),
		''
	);

	wp_enqueue_style(
		'fw-entypo',
		fw_get_framework_directory_uri( '/static/libs/entypo/css/entypo.css' ),
		array(),
		''
	);
}

wp_enqueue_style(
	'font_awesome',
	get_template_directory_uri() . '/css/font-awesome.min.css',
	array(),
	''
);

wp_enqueue_style(
	'bootstrap-css',
	get_template_directory_uri() . '/css/bootstrap.min.css',
	array(),
	'3.3.2'
);

wp_enqueue_style(
	'banquet-chinese-venobox-css',
	get_template_directory_uri() . '/css/venobox.css',
	array(),
	'2.2.0'
);

wp_enqueue_style(
	'banquet-chinese-slick-css',
	get_template_directory_uri() . '/css/slick.css',
	array(),
	'2.2.0'
);

// Load our main stylesheet.
wp_enqueue_style(
	'banquet-chinese-theme-style',
	get_stylesheet_uri(),
	array( 'genericons' ),
	'1.0'
);

// Load the Internet Explorer specific stylesheet.
wp_enqueue_style(
	'banquet_chinese-ie',
	get_template_directory_uri() . '/css/ie.css',
	array( 'banquet_chinese-style', 'genericons' ),
	'1.0'
);
wp_style_add_data( 'banquet_chinese-ie', 'conditional', 'lt IE 9' );

if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
	wp_enqueue_script( 'comment-reply' );
}

if ( is_singular() && wp_attachment_is_image() ) {
	wp_enqueue_script(
		'banquet_chinese-keyboard-image-navigation',
		get_template_directory_uri() . '/js/keyboard-image-navigation.js',
		array( 'jquery' ),
		'1.0'
	);
}

if ( is_front_page() && 'slider' == get_theme_mod( 'featured_content_layout' ) ) {
	wp_enqueue_script(
		'banquet_chinese-slider',
		get_template_directory_uri() . '/js/slider.js',
		array( 'jquery' ),
		'1.0',
		true
	);
	wp_localize_script( 'banquet_chinese-slider', 'featuredSliderDefaults', array(
		'prevText' => esc_attr__( 'Previous', 'banquetchinese' ),
		'nextText' => esc_attr__( 'Next', 'banquetchinese' )
	) );
}

/**
 * Enqueue scripts for the front end.
 */

// Bootstrap
{
	wp_enqueue_script(
		'bootstrap',
		get_template_directory_uri() . '/js/bootstrap.min.js',
		array(),
		'3.3.6',
		true
	);
}

// Headhesive
{
	wp_enqueue_script(
		'banquet-chinese-headhesive',
		get_template_directory_uri() . '/js/headhesive.min.js',
		array(),
		'1.2.4',
		true
	);
}

// Modernizr
{
	wp_enqueue_script(
		'banquet-chinese-modernizr',
		get_template_directory_uri() . '/js/modernizr.custom.js',
		array(),
		'2.5.3',
		true
	);
}

// Slick carousel
{
	wp_enqueue_script(
		'banquet-chinese-slick',
		get_template_directory_uri() . '/js/slick.min.js',
		array(),
		'1.6.0',
		false
	);
}

// Venobox

{
	wp_enqueue_script(
		'banquet-chinese-venobox',
		get_template_directory_uri() . '/js/venobox.min.js',
		array(),
		'1.7.3',
		true
	);
}

// Scrollme js

{
	wp_enqueue_script(
		'banquet-chinese-scrollme',
		get_template_directory_uri() . '/js/scrollme.js',
		array(),
		'1.0',
		true
	);

}

// Custom js

{
	wp_enqueue_script(
		'banquet-chinese-custom-js',
		get_template_directory_uri() . '/js/custom.js',
		array(),
		'1.0',
		true
	);

}

{
	wp_enqueue_script(
		'html5',
		get_template_directory_uri() . '/js/html5shiv.min.js',
		array(),
		'1.0',
		true
	);

	wp_script_add_data( 'html5', 'conditional', 'lt IE 10' );
}

{
	wp_enqueue_script(
		'respond',
		get_template_directory_uri() . '/js/respond.min.js',
		array(),
		'1.0',
		true
	);

	wp_script_add_data( 'respond', 'conditional', 'lt IE 10' );
}