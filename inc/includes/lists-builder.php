<?php

/** @internal */
function _action_include_demo_lists_builder() {
    if (!fw_ext('builder')) {
        /**
         * Lists Builder requires the FW_Option_Type_Builder class
         * which does not exist if the 'builder' extension is not active.
         *
         * You can install and activate the 'builder' extension by installing any extension that uses it,
         * for e.g. Page Builder or Learning (which has the Learning Quiz Builder sub-extension)
         */
        return;
    }

    require_once get_template_directory() . '/inc/includes/option-types/lists-builder/class-fw-option-type-lists-builder.php';
}
add_action('fw_option_types_init', '_action_include_demo_lists_builder');

?>