<?php

/*
 * Add Featured Content functionality.
 *
 * To overwrite in a plugin, define your own banquet_chinese_Featured_Content class on or
 * before the 'setup_theme' hook.
 */
if ( ! class_exists( 'banquet_chinese_Featured_Content' ) && 'plugins.php' !== $GLOBALS['pagenow'] ) {
	require get_template_directory() . '/inc/includes/sub-includes/featured-content.php';
}