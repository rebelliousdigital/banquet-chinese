<?php
	$setting = ( function_exists( 'fw_get_db_post_option' ) ) ? fw_get_db_settings_option() : '';
?>

<div class="topbar">
	<div class="container">
		<div class="row">
			<div class="col-sm-5">
				<?php echo wp_kses_post($setting['contact']); ?>
			</div>
			<div class="col-sm-7 contact-info">
				<?php if ($setting['email']) { ?>
					<ul>
						<li>
							<span><?php echo esc_attr__('EMAIL : '); ?></span>
							<a href="mailto:<?php echo esc_attr($setting['email']); ?>">
								<?php echo esc_attr($setting['email']); ?>
							</a>
						</li>
					</ul>
				<?php } ?>
				<?php if ($setting['tel']) { ?>
					<ul>
						<li>
							<span><?php echo esc_attr__('CALL : '); ?></span>
							<a href="tel:<?php echo str_replace(' ', '', $setting['tel']); ?>">
								<?php echo esc_attr($setting['tel']); ?>
							</a>
						</li>
					</ul>
				<?php } ?>
			</div>
		</div>
	</div>
</div>

<div class="navbar" role="navigation">

	<div class="container">
		<div class="navbar-header row">
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
				<span class="sr-only"><?php echo esc_attr__('Toggle navigation', 'banquetchinese'); ?></span>
				<span class="icon-bar top-bar"></span>
				<span class="icon-bar middle-bar"></span>
				<span class="icon-bar bottom-bar"></span>
			</button>
			<div class="col-sm-4 left-menu">
				<div class="collapse navbar-collapse">
					<?php
						wp_nav_menu( array(
							'menu'              => 'left',
							'theme_location'    => 'left',
							'depth'             => 2,
							'menu_class'        => 'nav navbar-nav',
							'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
							'walker'            => new wp_bootstrap_navwalker())
						);
					?>
				</div>
			</div>
			<div class="col-sm-4 logo-wrap">
				<?php if( !empty($setting['logo']) ) : ?>
					<a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php bloginfo('name'); ?>" class="logo">
						<img src="<?php echo esc_url($setting['logo']['url']) ?>" alt="<?php bloginfo('name'); ?>">
					</a>
				<?php else : ?>
					<a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php bloginfo('name'); ?>" class="text-logo">
						<span class="site-name"><?php bloginfo('name'); ?></span>
						<span class="site-tagline"><?php bloginfo('description'); ?></span>
					</a>
				<?php endif ?>
			</div>
			<div class="col-sm-4 right-menu">
				<div class="collapse navbar-collapse">
					<?php
						wp_nav_menu( array(
							'menu'              => 'right',
							'theme_location'    => 'right',
							'depth'             => 2,
							'menu_class'        => 'nav navbar-nav',
							'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
							'walker'            => new wp_bootstrap_navwalker())
						);
					?>
				</div>
			</div>
		</div>
	</div>
</div>