<?php
/**
 * The Template for displaying all single posts
 */

get_header();

$setting = ( function_exists( 'fw_get_db_settings_option' ) ) ? fw_get_db_settings_option() : '';
get_template_part('inc/header', 'nav');
?>

<?php if (!empty($setting['pageTitle'])) { ?>
	<div class="page-title scrollme"
		<?php echo !empty($setting['pageBg']) ? 'style="background: url('. esc_url($setting['pageBg']['url']) .');"' : ''; ?>
	>
		<div class="container">
			<div class="row">
				<div class="col-sm-8 col-sm-push-2 animateme" data-when="span" data-from="0" data-to="1" data-translatey="-200" data-opacity="0">
					<h1><?php echo esc_attr($setting['pageTitle']); ?></h1>
					<?php if (!empty($setting['pageDesc'])) {
						echo '<p>'. wp_kses_post($setting['pageDesc']) .'</p>';
					} ?>
				</div>
			</div>
		</div>
	</div>
<?php } ?>

<div class="container-fluid section">
	<div class="row">

		<div class="col-sm-12">

				<?php
					// Start the Loop.
					while ( have_posts() ) : the_post();

						/*
						 * Include the post format-specific template for the content. If you want to
						 * use this in a child theme, then include a file called called content-___.php
						 * (where ___ is the post format) and that will be used instead.
						 */
						get_template_part( 'excerpt', get_post_format('') );

						// Previous/next post navigation.
						// fw_theme_post_nav();

						// If comments are open or we have at least one comment, load up the comment template.
						if ( comments_open() || get_comments_number() ) {
							comments_template();
						}

					endwhile; ?>
		</div>

	</div>
	<div class="row">
		<div class="col-sm-12">
			<?php banquet_chinese_paging_nav(); ?>
		</div>
	</div>
</div>

<?php
get_footer();