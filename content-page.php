<?php
/**
 * The template used for displaying page content
 */

the_content();

wp_link_pages( array(
	'before'      => '<div class="page-links"><span class="page-links-title">' . esc_attr__( 'Pages:', 'banquetchinese' ) . '</span>',
	'after'       => '</div>',
	'link_before' => '<span>',
	'link_after'  => '</span>',
) );

edit_post_link( esc_attr__( 'Edit', 'banquetchinese' ), '<span class="edit-link">', '</span>' );
