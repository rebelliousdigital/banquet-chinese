<?php
/**
 * The Template for displaying all single posts
 */

get_header();

$setting = ( function_exists( 'fw_get_db_settings_option' ) ) ? fw_get_db_settings_option() : '';
$category = get_the_category();

get_template_part('inc/header', 'nav');

?>

<div class="page-title scrollme" style="background-image: url(<?php the_post_thumbnail_url($size = 'full'); ?>);">
	<div class="container">
		<div class="row">
			<div class="col-sm-8 col-sm-push-2 animateme" data-when="span" data-from="0" data-to="1" data-translatey="-200" data-opacity="0">
				<span class="cat"><?php echo $firstCategory = $category[0]->cat_name; ?></span>
				<h1><?php the_title(); ?></h1>
			</div>
		</div>
	</div>
</div>

<div class="container single-wrap">
	<div class="row">
		<div class="col-sm-8">
			<?php
				// Start the Loop.
				while ( have_posts() ) : the_post();

					/*
					 * Include the post format-specific template for the content. If you want to
					 * use this in a child theme, then include a file called called content-___.php
					 * (where ___ is the post format) and that will be used instead.
					 */
					get_template_part( 'content', get_post_format() );

					// Previous/next post navigation.

					// If comments are open or we have at least one comment, load up the comment template.
					

				endwhile;
			?>
		</div>
		<div class="col-sm-3 col-sm-push-1" id="sidebar">
			<?php dynamic_sidebar('news-sidebar'); ?>
		</div>
	</div>
</div>

<?php get_footer(); ?>