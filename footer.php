<?php
/**
 * The template for displaying the footer
 *
 * Contains footer content and the closing of the #main and #page div elements.
 */
$social = ( function_exists( 'fw_get_db_settings_option' ) ) ? fw_get_db_settings_option('socialURL') : '';

$setting = ( function_exists( 'fw_get_db_post_option' ) ) ? fw_get_db_settings_option() : '';
?>
	<div class="container-fluid cta-btns">
		<div class="row">
			<div class="col-sm-4 btn-1">
				<a href="<?php echo esc_url(get_the_permalink($setting['footerbuttonanchor1'][0])); ?>">
					<?php echo esc_attr($setting['footerbutton1']); ?>
				</a>
			</div>
			<div class="col-sm-4 btn-2">
				<a href="<?php echo esc_url(get_the_permalink($setting['footerbuttonanchor2'][0])); ?>">
					<?php echo esc_attr($setting['footerbutton2']); ?>
				</a>
			</div>
			<div class="col-sm-4 btn-3">
				<a href="<?php echo esc_url(get_the_permalink($setting['footerbuttonanchor3'][0])); ?>">
					<?php echo esc_attr($setting['footerbutton3']); ?>
				</a>
			</div>
		</div>
	</div>

	<footer>
		<div class="container">
			<div class="row">
				<div class="col-sm-4 left">
					<?php
						if ( is_active_sidebar( 'ft_col_1' ) ) :
							dynamic_sidebar('ft_col_1');
						endif;
					?>
				</div>
				<div class="col-sm-4 middle">
					<?php
						if ( is_active_sidebar( 'ft_col_2' ) ) :
							dynamic_sidebar('ft_col_2');
						endif;
					?>

					<a href="<?php echo get_the_permalink(17) ?>" class="btn ghost">
						<?php echo __('Online bookings','banquetchinese'); ?>
					</a>

					<ul class="social">
					 <?php 
					    if (is_array($social) || is_object($social)) {
					      foreach ( $social as $social ) : ?>

					      <?php if ($social['icon']['icon-class']) { ?>
					        <li>
					          <a href="<?php echo esc_url($social['url']); ?>" class="<?php echo esc_attr($social['icon']['icon-class']); ?>" title="<?php echo esc_attr($social['account']); ?>"></a>
					        </li>
					      <?php } elseif ($social['icon']['url']) { ?>
					        <li>  
					          <a href="<?php echo esc_url($social['url']); ?>" class="<?php echo esc_attr($social['icon']['icon-class']); ?>" title="<?php echo esc_attr($social['account']); ?>">
					              <img src="<?php echo esc_url($social['icon']['url']); ?>" alt="<?php echo esc_attr($heading); ?>" />
					          </a>
					        </li>
					      <?php } ?>

					    <?php 
					      endforeach;
					    } ?>
					</ul>
					<div class="copyright">
						<?php echo __( '<a href="https://www.klevermedia.co.uk">Web design</a> by Klever media', 'banquetchinese' ); ?>
					</div>
				</div>
				<div class="col-sm-4 right">
					<?php
						if ( is_active_sidebar( 'ft_col_3' ) ) :
							dynamic_sidebar('ft_col_3');
						endif;
					?>
				</div>
			</div>
		</div>
	</footer>

<?php if ( class_exists( 'WooCommerce' ) ) {
	echo "</div>";
} ?>

<a href="#" id="back-to-top">
	<i class="fa fa-chevron-up"></i>
</a>

<?php wp_footer(); ?>
</body>
</html>