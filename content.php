<div class="latest-post">
	
	<p class="time"><?php the_time('d F, Y'); ?></p>
	<h2><?php the_title(); ?></h2>
	<hr />

	<?php the_content(); ?>

	<span class="share"><?php echo esc_attr__( 'Share this:', 'banquetchinese' ); ?></span>
	<ul class="social-share">
		<li>
			<a href="http://www.facebook.com/sharer.php?u=<?php the_permalink(); ?>">
				<i class="fa fa-facebook"></i>
				<?php echo esc_attr__( 'Facebook', 'banquetchinese' ); ?>
			</a>
		</li>
		<li>
			<a href="http://www.linkedin.com/shareArticle?url=<?php the_permalink(); ?>&title=<?php echo urlencode(get_the_title()); ?>">
				<i class="fa fa-linkedin"></i>
				<?php echo esc_attr__( 'LinkedIn', 'banquetchinese' ); ?>
			</a>
		</li>
		<li>
			<a href="https://twitter.com/share?url=<?php the_permalink(); ?>&text=<?php echo urlencode(get_the_title()); ?>">
				<i class="fa fa-twitter"></i>
				<?php echo esc_attr__( 'Twitter', 'banquetchinese' ); ?>
			</a>
		</li>
	</ul>

</div>