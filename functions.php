<?php

/**
 * Theme Includes
 */
require_once get_template_directory() .'/inc/init.php';

/**
 * TGM Plugin Activation
 */
{
	require_once get_template_directory() . '/TGM-Plugin-Activation/class-tgm-plugin-activation.php';

	/** @internal */
	function banquet_chinese_register_required_plugins() {
		tgmpa( array(
			array(
				'name'      => esc_html__('unyson', 'banquetchinese'),
				'slug'      => esc_html__('unyson', 'banquetchinese'),
				'required'  => true,
				'force_activation'   => true,
			),
		));

	}
	add_action( 'tgmpa_register', 'banquet_chinese_register_required_plugins' );
}

/**
 * Register Custom Navigation Walker
 *
 */
require_once( trailingslashit( get_template_directory() ). 'inc/wp_bootstrap_navwalker.php' );


/**
 * Register widgets
 *
 */
if (function_exists('register_sidebar')) {
		register_sidebar(array(
			'name'=> esc_html__('News sidebar', 'banquetchinese'),
			'id' => 'news-sidebar',
			'before_widget' => '<div class="widget">',
			'after_widget' => '</div>',
			'before_title' => '<h4>',
			'after_title' => '</h4>',
		));
		register_sidebar(array(
			'name'=> esc_html__('Footer widget area 1', 'banquetchinese'),
			'id' => 'ft_col_1',
			'before_widget' => '',
			'after_widget' => '',
			'before_title' => '<h6>',
			'after_title' => '</h6>',
		));
		register_sidebar(array(
			'name'=> esc_html__('Footer widget area 2', 'banquetchinese'),
			'id' => 'ft_col_2',
			'before_widget' => '',
			'after_widget' => '',
			'before_title' => '<h6>',
			'after_title' => '</h6>',
		));
		register_sidebar(array(
			'name'=> esc_html__('Footer widget area 3', 'banquetchinese'),
			'id' => 'ft_col_3',
			'before_widget' => '',
			'after_widget' => '',
			'before_title' => '<h6>',
			'after_title' => '</h6>',
		));
		
}


/**
 * Custom comments template
 *
 */

function banquet_chinese_time_ago( $type = 'post' ) {
	$d = 'comment' == $type ? 'get_comment_time' : 'get_post_time';

	return human_time_diff($d('U'), current_time('timestamp')) . " " . esc_attr__('ago', 'banquetchinese');

}

function banquet_chinese_comment($comment, $args, $depth) {
	$GLOBALS['comment'] = $comment; ?>
		<div class="row scrollme animateme"  id="comment-<?php comment_ID() ?>" data-when="enter" data-from="0.75" data-to="0" data-opacity="0" data-translatey="75">
			<div class="col-sm-1">
				<?php echo get_avatar( $comment, 83 ); ?>
			</div>
			<div class="col-sm-11">
				<?php if ($comment->comment_approved == '0') : ?>
					<em><?php esc_attr__('Your comment is awaiting moderation.', 'banquetchinese') ?></em>
					<br />
				<?php endif; ?>

				<h5><?php printf(__('%s', 'banquetchinese'), get_comment_author_link()) ?></h5>

				<?php comment_text() ?>

				<span class="timeago">	
					<?php
					if ( get_comment_time('U') > date('U') - 7*60*60*24 ) {
						echo human_time_diff( get_comment_time('U'), current_time('timestamp') ) . ' ago';
						} elseif ( get_comment_date('Y') == date('Y') ) {
						comment_date('F j');
						} else {
						comment_date('F j, Y');
					} ?>
				</span>

				<span class="edit">
					<?php edit_comment_link(__(' Edit', 'banquetchinese'),'  ','') ?> <?php echo esc_attr__('|', 'banquetchinese' ); ?> 
					<?php comment_reply_link(array_merge( $args, array('depth' => $depth, 'max_depth' => $args['max_depth']))) ?>
				</span>

			</div>
		</div>
   <?php
}

add_theme_support('post-thumbnails');
add_theme_support('automatic-feed-links');
add_theme_support('custom-header');
add_theme_support( 'custom-background');

// Remove type attribute
add_filter('style_loader_tag', 'codeless_remove_type_attr', 10, 2);
add_filter('script_loader_tag', 'codeless_remove_type_attr', 10, 2);
function codeless_remove_type_attr($tag, $handle) {
    return preg_replace( "/type=['\"]text\/(javascript|css)['\"]/", '', $tag );
}

// Uploas SVGs
function banquet_chinese_mime_types($mimes) {
  $mimes['svg'] = 'image/svg+xml';
  return $mimes;
}
add_filter('upload_mimes', 'banquet_chinese_mime_types');

// Stop excerpt at first full stop
add_filter(
  'the_excerpt',
  function ($excerpt) {
    return substr($excerpt,0,strpos($excerpt,'.')+1);
  }
);