<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other 'pages' on your WordPress site will use a different template.
 */

get_header();

$page = ( function_exists( 'fw_get_db_post_option' ) ) ? fw_get_db_post_option() : '';
get_template_part('inc/header', 'nav');
?>

<?php if (!empty($page['pageTitle'])) { ?>
	<div class="page-title scrollme"
		<?php echo !empty($page['pageBg']) ? 'style="background-image: url('. esc_url($page['pageBg']['url']) .');"' : ''; ?>
	>
		<div class="container">
			<div class="row">
				<div class="col-sm-8 col-sm-push-2 animateme" data-when="span" data-from="0" data-to="1" data-translatey="-200" data-opacity="0">
					<h1><?php echo esc_attr($page['pageTitle']); ?></h1>
					<?php if (!empty($page['pageDesc'])) {
						echo '<p>'. wp_kses_post($page['pageDesc']) .'</p>';
					}

					?>
				</div>
				<?php if (!empty($page['icons'])) { ?>
					<div class="row">
						<div class="col-sm-10 col-sm-push-1 animateme" data-when="span" data-from="0" data-to="1.25" data-translatey="-150" data-opacity="0">
							<ul class="icon-group">
							 	<?php foreach ( $page['icons'] as $icon ) : ?>

							      <?php if ($icon['icon']['icon-class']) { ?>
							        <li>
							          <div class="icon-row">
										<div class="icon">
											<i class="<?php echo esc_attr($icon['icon']['icon-class']); ?>"></i>
										</div>
										<div class="content">
											<?php echo wp_kses_post($icon['content']); ?>
										</div>
									</div>
							        </li>
							      <?php } ?>

							    <?php  endforeach; ?>
							</ul>
						</div>
					</div>
				<?php } ?>
			</div>
		</div>
	</div>
<?php } ?>

<?php if (defined('FW')) { FW_Flash_Messages::_print_frontend(); } ?>

<?php
	while ( have_posts() ) : the_post();

		// Include the page content template.
		get_template_part( 'content', 'page' );
		
		// If comments are open or we have at least one comment, load up the comment template.
		/*if ( comments_open() || get_comments_number() ) {
			comments_template();
		}*/
					
	endwhile;
?>


<?php
get_footer();