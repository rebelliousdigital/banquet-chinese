<?php
/**
 * The template for displaying Comments
 *
 * The area of the page that contains comments and the comment form.
 */

/*
 * If the current post is protected by a password and the visitor has not yet
 * entered the password we will return early without loading the comments.
 */
if ( post_password_required() ) {
	return;
}
?>

<div id="comments" class="comments-area comments">

	<?php if ( have_comments() ) : ?>

	<h3>
		<?php
			printf( _n( '1 comment', '%1$s comments', get_comments_number(), 'banquetchinese' ),
				number_format_i18n( get_comments_number() ), get_the_title() );
		?>
	</h3>

	<?php if ( get_comment_pages_count() > 1 && get_option( 'page_comments' ) ) : ?>
	<nav id="comment-nav-above" class="navigation comment-navigation" role="navigation">
		<h1 class="screen-reader-text"><?php esc_html_e( 'Comment navigation', 'banquetchinese' ); ?></h1>
		<div class="nav-previous"><?php previous_comments_link( esc_attr__( '&larr; Older Comments', 'banquetchinese' ) ); ?></div>
		<div class="nav-next"><?php next_comments_link( esc_attr__( 'Newer Comments &rarr;', 'banquetchinese' ) ); ?></div>
	</nav><!-- #comment-nav-above -->
	<?php endif; // Check for comment navigation. ?>

	<?php
		// only pingbacks
        wp_list_comments( array( 'type' => 'pingback', 'callback' => 'banquet_chinese_comment' ) );

        // only comments
        wp_list_comments( array( 'type' => 'comment', 'callback' => 'banquet_chinese_comment' ) );
	?>

	<?php if ( get_comment_pages_count() > 1 && get_option( 'page_comments' ) ) : ?>
	<nav id="comment-nav-below" class="navigation comment-navigation" role="navigation">
		<h1 class="screen-reader-text"><?php esc_html_e( 'Comment navigation', 'banquetchinese' ); ?></h1>
		<div class="nav-previous"><?php previous_comments_link( esc_attr__( '&larr; Older Comments', 'banquetchinese' ) ); ?></div>
		<div class="nav-next"><?php next_comments_link( esc_attr__( 'Newer Comments &rarr;', 'banquetchinese' ) ); ?></div>
	</nav><!-- #comment-nav-below -->
	<?php endif; // Check for comment navigation. ?>

	<?php if ( ! comments_open() ) : ?>
	<p class="no-comments"><?php esc_html_e( 'Comments are closed.', 'banquetchinese' ); ?></p>
	<?php endif; ?>

	<?php endif; // have_comments() ?>

	<?php comment_form(); ?>

</div><!-- #comments -->
