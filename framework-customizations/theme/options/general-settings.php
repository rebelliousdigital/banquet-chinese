<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$options = array(
	'general' => array(
		'title'   => esc_html__( 'General', 'banquetchinese' ),
		'type'    => 'tab',
		'options' => array(
			'general-box' => array(
				'title'   => esc_html__( 'General Settings', 'banquetchinese' ),
				'type'    => 'box',
				'options' => array(
					'logo' => array(
						'label' => esc_html__( 'Logo', 'banquetchinese' ),
						'desc'  => esc_html__( 'Upload a logo', 'banquetchinese' ),
						'type'  => 'upload'
					),
					'userCSS'  => array(
						'label' => esc_html__( 'Custom css', 'banquetchinese' ),
						'type'  => 'textarea',
						'value' => false,
						'desc'  => esc_html__( 'You can add custom css to modify the theme here.', 'banquetchinese' )
					),
				)
			),
		)
	)
);