<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$options = array(
	'blog' => array(
		'title'   => esc_attr__( 'News', 'banquetchinese' ),
		'type'    => 'tab',
		'options' => array(
			'blog-main' => array(
				'title'   => esc_attr__( 'News Settings', 'banquetchinese' ),
				'type'    => 'box',
				'options' => array(
					'heading-group' => array(
						'type'    => 'group',
						'options' => array(
							'pageTitle' 	=> array(
								'label'   		=> esc_attr__( 'Title', 'banquetchinese' ),
								'type'    		=> 'text',
								'desc'    		=> false
							),
							'pageDesc' 	=> array(
								'label'   		=> esc_attr__( 'Description', 'banquetchinese' ),
								'type'    		=> 'textarea',
								'desc'    		=> false
							),
						),
					),
					'pageBg' 	=> array(
						'label'   		=> esc_attr__( 'Background', 'banquetchinese' ),
						'type'    		=> 'upload',
						'desc'    		=> esc_attr__( 'Upload header background image.', 'banquetchinese' ),
					),
				)
			),
		),
	)
);