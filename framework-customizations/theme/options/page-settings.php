<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$options = array(
	'service' => array(
		'title' => esc_html__( 'Page Settings', 'banquetchinese' ),
		'type'  => 'tab',
		'priority' => 'high',
		'context' => 'side',
		'options' => array(
			'service-box' => array(
				'title'   => esc_html__( 'Header Settings', 'banquetchinese' ),
				'type'    => 'box',
				'options' => array(
					'heading-group' => array(
						'type'    => 'group',
						'options' => array(
							'pageTitle' 	=> array(
								'label'   		=> esc_attr__( 'Title', 'banquetchinese' ),
								'type'    		=> 'text',
								'desc'    		=> false
							),
							'pageDesc' 	=> array(
								'label'   		=> esc_attr__( 'Description', 'banquetchinese' ),
								'type'    		=> 'textarea',
								'desc'    		=> false
							),
						),
					),
					'pageBg' 	=> array(
						'label'   		=> esc_attr__( 'Background', 'banquetchinese' ),
						'type'    		=> 'upload',
						'desc'    		=> esc_attr__( 'Upload header background image.', 'banquetchinese' ),
					),
					'icons' => array(
						'type' => 'addable-popup',
					    'label' => esc_html__('Icon groups', 'silverbluff'),
					    'desc'  => esc_html__('Add icon group', 'silverbluff'),
					    'popup-title' => esc_html__('Icons','silverbluff'),
					    'limit' => 3,
					    'add-button-text' => esc_html__('Add icons', 'silverbluff'),
					    'template' => '{{- content }}',
					    'popup-options' => array(
					    	'icon'    => array(
								'type'  => 'icon-v2',
								'label' => esc_attr__('Choose an Icon', 'banquetchinese'),
							),
							'content' => array(
								'type'  => 'wp-editor',
								'label' => esc_attr__( 'Content', 'banquetchinese' )
							), 
					    ),
					),
				)
			),
		)
	)
);