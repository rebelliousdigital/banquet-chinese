<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}


$options = array(
	'footer' => array(
		'title'   => esc_attr__( 'Footer', 'banquetchinese' ),
		'type'    => 'tab',
		'options' => array(
			'general-box' => array(
				'title'   => esc_attr__( 'Footer Settings', 'banquetchinese' ),
				'type'    => 'box',
				'options' => array(
					'socialURL' => array(
						'type' => 'addable-popup',
					    'label' => esc_html__('Social profiles', 'silverbluff'),
					    'desc'  => esc_html__('Add your social media profile accounts.', 'silverbluff'),
					    'template' => '{{- account }}',
					    'popup-title' => esc_html__('Social profiles','silverbluff'),
					    'size' => 'small',
					    'limit' => 0,
					    'add-button-text' => esc_html__('Add profiles', 'silverbluff'),
					    'sortable' => true,
					    'popup-options' => array(
					    	'icon'    => array(
								'type'  => 'icon-v2',
								'modal_size' => 'medium',
								'preview_size' => 'large',
								'label' => esc_html__('Choose an Icon', 'silverbluff'),
							),
							'account'   => array(
								'type'  => 'text',
								'label' => esc_html__('Title', 'silverbluff')
							),
							'url' => array(
								'type'  => 'text',
								'value' => esc_html__('http://', 'silverbluff'),
								'label' => esc_html__('URL', 'silverbluff')
							) 
					    ),
					),
					'button1_group' => array(
   						'type'    => 'group',
   						'options' => array(
							'footerbutton1'   => array(
								'type'  => 'text',
								'label' => esc_html__('Left button', 'banquetchinese'),
								'value' => esc_html__('Our food menu', 'banquetchinese'),
								'desc' => esc_html__('Left button title text.', 'banquetchinese')
							),
							'footerbuttonanchor1'  => array(
							    'type'       	=> 'multi-select',
								'label'      	=> esc_attr__( 'Link', 'silverbluff' ),
								'population' 	=> 'posts',
								'source'     	=> 'page',
								'limit' 	 	=> 1,
								'prepopulate' 	=> 999,
								'desc'       	=> esc_attr__('Select a page to link to.', 'silverbluff'),
							),
						),
					),
					'button2_group' => array(
   						'type'    => 'group',
   						'options' => array(
							'footerbutton2'   => array(
								'type'  => 'text',
								'label' => esc_html__('Middle button', 'banquetchinese'),
								'value' => esc_html__('Book a table', 'banquetchinese'),
								'desc' => esc_html__('Middle button title text.', 'banquetchinese')
							),
							'footerbuttonanchor2'  => array(
							    'type'       	=> 'multi-select',
								'label'      	=> esc_attr__( 'Link', 'silverbluff' ),
								'population' 	=> 'posts',
								'source'     	=> 'page',
								'limit' 	 	=> 1,
								'prepopulate' 	=> 999,
								'desc'       	=> esc_attr__('Select a page to link to.', 'silverbluff'),
							),
						),
					),
					'button3_group' => array(
   						'type'    => 'group',
   						'options' => array(
							'footerbutton3'   => array(
								'type'  => 'text',
								'label' => esc_html__('Right button', 'banquetchinese'),
								'value' => esc_html__('Where to find us', 'banquetchinese'),
								'desc' => esc_html__('Right button title text.', 'banquetchinese')
							),
							'footerbuttonanchor3'  => array(
							    'type'       	=> 'multi-select',
								'label'      	=> esc_attr__( 'Link', 'silverbluff' ),
								'population' 	=> 'posts',
								'source'     	=> 'page',
								'limit' 	 	=> 1,
								'prepopulate' 	=> 999,
								'desc'       	=> esc_attr__('Select a page to link to.', 'silverbluff'),
							),
						),
					),
				)
			),
		)
	)
);