<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$options = array(
	'header' => array(
		'title'   => esc_html__( 'Header', 'banquetchinese' ),
		'type'    => 'tab',
		'options' => array(
			'general-box' => array(
				'title'   => esc_html__( 'Header Settings', 'banquetchinese' ),
				'type'    => 'box',
				'options' => array(
					'contact'   => array(
						'type'  => 'wp-editor',
						'label' => esc_html__('Opening hours', 'banquetchinese')
					),
					'contact_group' => array(
   						'type'    => 'group',
   						'options' => array(
							'email'   => array(
								'type'  => 'text',
								'label' => esc_html__('Email', 'banquetchinese')
							),
							'tel'   => array(
								'type'  => 'text',
								'label' => esc_html__('Telephone', 'banquetchinese')
							),
						),
					),
				)
			),
		)
	)
);