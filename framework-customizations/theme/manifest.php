<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$manifest = array();

$manifest['id'] = 'banquetchinese';

$manifest['supported_extensions'] = array(
	'page-builder' => array(),
	'backups' => array(),
	'slider' => array(),
	'analytics' => array(),
	'breadcrumbs' => array(),
	/*'portfolio' => array(),
	'events' => array(),
	'feedback' => array(),
	'learning' => array(),
	'megamenu' => array(),
	'sidebars' => array(),*/
);