<?php if ( ! defined( 'FW' ) ) { die( 'Forbidden' ); } ?>

<div id="hero" class="carousel slide carousel-fade section no-padding" data-ride="carousel">
	<?php if ( isset( $data['slides'] ) && !empty($data['slides']) ) :?>
		<?php
		$interval = !empty($data['settings']['extra']['interval']) ? 'interval:'.(int)$data['settings']['extra']['interval'].' ' : 'interval:5000'; ?>
			<!-- Indicators -->
			<ol class="carousel-indicators">
				<?php foreach ( $data['slides'] as $key => $slide ) : ?>
				    <li data-target="#hero" data-slide-to="<?php echo ($key + 0); ?>" <?php echo ($key == 0) ? 'class="active"' : '';?>></li>
				<?php endforeach ?>
			</ol>

			<!-- Wrapper for slides -->
			<div class="carousel-inner" role="listbox">
				<!-- Slide one -->
				<?php $i = 0; foreach($data['slides'] as $slide): $i++;?>
					<?php if(empty($slide['src'])) continue; ?>
					<div class="item <?php echo ($i == 1) ? 'active' : '';?>" style="<?php echo 'background: url(' . esc_url($slide['src']) .');'; ?>">

						<div class="container">
							<div class="row blurb">
                				<div class="col-md-8 col-md-push-2">
									<h1><?php echo sanitize_text_field($slide['title']);?></h1>
									
									<a href="<?php echo esc_url(get_the_permalink($slide['extra']['anchor1'][0])); ?>" class="btn ">
										<?php echo esc_attr($slide['extra']['btntitle1']); ?>
									</a>

									<a href="<?php echo esc_url(get_the_permalink($slide['extra']['anchor2'][0])); ?>" class="btn ghost">
										<?php echo esc_attr($slide['extra']['btntitle2']); ?>
									</a>

								</div>
							</div>
						</div>

					</div>
				<?php endforeach;?>
			<!-- Slides end -->
			</div>

			<script>
				jQuery(document).ready(function($){
					// Hero slider
					$('#hero').carousel({
						<?php echo ($interval);?>,
						keyboard: true,
						swipe: 30,
						<?php if($data['settings']['extra']['hover_pause'] == 'yes'){ ?>
							pause: 'hover'
						<?php } ?>
					});
				});
			</script>
	<?php endif; ?>
</div>