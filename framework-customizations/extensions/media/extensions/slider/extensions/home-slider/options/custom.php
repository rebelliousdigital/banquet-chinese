<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}
$options = array(

    'btn_1' => array(
		'type'    => 'group',
		'options' => array(
			'btntitle1'   => array(
				'type'  => 'text',
				'label' => esc_html__('Button title', 'banquetchinese')
			),
			'anchor1'  => array(
			    'type'       	=> 'multi-select',
				'label'      	=> esc_attr__( 'Link', 'silverbluff' ),
				'population' 	=> 'posts',
				'source'     	=> 'page',
				'limit' 	 	=> 1,
				'prepopulate' 	=> 999,
				'desc'       	=> esc_attr__('Select a page to link to.', 'silverbluff'),
			),
		),
	),
	'btn_2' => array(
		'type'    => 'group',
		'options' => array(
			'btntitle2'   => array(
				'type'  => 'text',
				'label' => esc_html__('Button title', 'banquetchinese')
			),
			'anchor2'  => array(
			    'type'       	=> 'multi-select',
				'label'      	=> esc_attr__( 'Link', 'silverbluff' ),
				'population' 	=> 'posts',
				'source'     	=> 'page',
				'limit' 	 	=> 1,
				'prepopulate' 	=> 999,
				'desc'       	=> esc_attr__('Select a page to link to.', 'silverbluff'),
			),
		),
	),

);