<?php if (!defined('FW')) die('Forbidden');

$options = array(
    'interval' => array(
        'type'  => 'text',
        'label' => esc_html__( 'Interval','banquetchinese' ),
        'value' => '5000',
        'desc'  => esc_html__( 'Enter slider interval','banquetchinese' )
    ),
    'hover_pause'                    => array(
		'label'        => __( 'Pause slide', 'banquetchinese' ),
		'type'         => 'switch',
		'right-choice' => array(
			'value' => 'yes',
			'label' => esc_html__( 'Yes', 'banquetchinese' )
		),
		'left-choice'  => array(
			'value' => 'no',
			'label' => esc_html__( 'No', 'banquetchinese' )
		),
		'value'        => 'yes',
		'desc'         => esc_html__( 'Puase slide on hover?.',
			'banquetchinese' ),
	),
);