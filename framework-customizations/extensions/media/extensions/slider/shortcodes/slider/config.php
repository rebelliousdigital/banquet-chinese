<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$cfg = array();

$cfg['page_builder'] = array(
	'title'       => esc_attr__( 'Hero slider', 'banquetchinese' ),
	'description' => esc_attr__( 'Add a Hero slider', 'banquetchinese' ),
	'tab'         => esc_attr__( 'Media Elements', 'banquetchinese' ),
);
