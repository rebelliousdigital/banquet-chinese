<?php if (!defined('FW')) die('Forbidden');

/** @internal */
function _filter_disable_shortcodes($to_disable)
{
	
	$to_disable[] = 'calendar';
	$to_disable[] = 'map';
	$to_disable[] = 'icon';
	$to_disable[] = 'table';
	$to_disable[] = 'call_to_action';
	$to_disable[] = 'testimonials';
	$to_disable[] = 'notification';
	$to_disable[] = 'icon_box';
	$to_disable[] = 'accordion';
	return $to_disable;
}
add_filter('fw_ext_shortcodes_disable_shortcodes', '_filter_disable_shortcodes');