<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$options = array(
	'img-group' => array(
		'type'    => 'group',
		'options' => array(
			'alignment'			=> array(
				'label'        => __( 'Switch', 'unyson' ),
				'type'         => 'switch',
				'right-choice' => array(
					'value' => 'left',
					'label' => __( 'Left', 'unyson' )
				),
				'left-choice'  => array(
					'value' => 'right',
					'label' => __( 'Right', 'unyson' )
				),
				'value'        => 'left',
				'desc'   => __( 'Select image alignment.', 'banquetchinese' )
			),
			'image' => array(
				'type'   => 'upload',
				'label'  => __( 'Upload', 'banquetchinese' ),
				'desc'   => __( 'Upload a featured image.', 'banquetchinese' )
			),
		),
	),
	'text-group' => array(
		'type'    => 'group',
		'options' => array(
			'small-text' => array(
				'label' => __('Small header', 'banquetchinese'),
				'type'  => 'text',
			),
			'large-text' => array(
				'label' => __('Large header', 'banquetchinese'),
				'type'  => 'text',
			),
			'content' => array(
				'label' => __('Content', 'banquetchinese'),
				'type'  => 'wp-editor',
			),
		),
	),
);