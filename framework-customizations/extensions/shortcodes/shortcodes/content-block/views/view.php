<?php if ( ! defined( 'FW' ) ) { die( 'Forbidden' ); } ?>

<div class="content-block scrollme <?php echo esc_attr($atts['alignment']); ?>">
	<div class="image animateme" data-when="span" data-from="0" data-to="1" data-translatey="100">
		<img src="<?php echo esc_url($atts['image']['url']); ?>" alt="" />
	</div>
	<div class="content animateme" data-when="span" data-from="1" data-to="0" data-translatey="100">
		<header>
			<?php echo esc_attr($atts['small_text']); ?>
			<h3><?php echo esc_attr($atts['large_text']); ?></h3>
		</header>
		<?php echo wp_kses_post($atts['content']); ?>
	</div>
</div>