<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$cfg = array();

$cfg['page_builder'] = array(
	'title'       => __( 'Content Block', 'banquetchinese' ),
	'description' => __( 'Add a Content Block', 'banquetchinese' ),
	'tab'         => __( 'Content Elements', 'banquetchinese' ),
);
