<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$options = array(
	'posts_number' => array(
		'type'  => 'text',
		'label' => esc_attr__('Posts to display', 'banquetchinese'),
		'value' => '6',
		'help'  => esc_attr__( 'How many posts to display in the carousel', 'banquetchinese' ),
	),
	'read_more' => array(
		'type'  => 'text',
		'label' => esc_attr__('Read more label', 'banquetchinese'),
		'value' => 'Read on &hellip;',
	),
	'slide-settings-group' => array(
		'type'    => 'group',
		'options' => array(
			'large' => array(
				'type'  => 'text',
				'label' => esc_attr__('Large screen', 'banquetchinese'),
				'value' => '4',
				'help'  => esc_attr__( 'Enter how many slides should be visible at this screen size.', 'banquetchinese' ),
			),
			'medium' => array(
				'type'  => 'text',
				'label' => esc_attr__('Medium screen', 'banquetchinese'),
				'value' => '3',
				'help'  => esc_attr__( 'Enter how many slides should be visible at this screen size.', 'banquetchinese' ),
			),
			'small' => array(
				'type'  => 'text',
				'label' => esc_attr__('Tablet', 'banquetchinese'),
				'value' => '2',
				'help'  => esc_attr__( 'Enter how many slides should be visible at this screen size.', 'banquetchinese' ),
			),
			'xsmall' => array(
				'type'  => 'text',
				'label' => esc_attr__('Mobile', 'banquetchinese'),
				'value' => '1',
				'help'  => esc_attr__( 'Enter how many slides should be visible at this screen size.', 'banquetchinese' ),
			),
		),
	),
);