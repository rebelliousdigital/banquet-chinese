<?php if (!defined('FW')) die('Forbidden');

$cfg = array();

$cfg['page_builder'] = array(
	'title'         => esc_attr__('News carousel', 'banquetchinese'),
	'description'   => esc_attr__('Add a news carousel', 'banquetchinese'),
	'tab'           => esc_attr__('Content Elements', 'banquetchinese'),
);