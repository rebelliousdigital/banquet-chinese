<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}
/**
 * @var array $atts
 */
?>

<div class="slide latest-news scrollme">
	<?php
	$args = array('posts_per_page' => $atts['posts_number']); $the_query = new WP_Query( $args );

	while ( $the_query->have_posts() ) :
	    $the_query->the_post(); ?>
			<article>
				<a href="<?php the_permalink(); ?>">
					<?php the_post_thumbnail(); ?>
				</a>
				<div class="article-content animateme" data-when="span" data-from="1" data-to="0" data-translatey="100">
					<p class="time"><?php the_time('d F, Y'); ?></p>
					<h4><?php the_title(); ?></h4>
					<?php the_excerpt(); ?>
					<p><a href="<?php the_permalink(); ?>"><?php echo esc_attr($atts['read_more']); ?></a></p>
				</div>
			</article>    
	<?php endwhile; ?>
</div>
<script>
	jQuery(document).ready(function($){
		jQuery('.latest-news').slick({
			infinite: true,
			slidesToShow: <?php echo esc_attr($atts['large']); ?>,
			slidesToScroll: 1,
			autoplay: true,
			autoplaySpeed: 3000,
  			arrows: false,
  			nextArrow: '<i class="fa fa-chevron-right"></i>',
				prevArrow: '<i class="fa fa-chevron-left"></i>',
  			responsive: [
			    {
			      breakpoint: 991,
			      settings: {
			        slidesToShow: <?php echo esc_attr($atts['medium']); ?>,
			        slidesToScroll: 1
			      }
			    },
			    {
			      breakpoint: 768,
			      settings: {
			        slidesToShow: <?php echo esc_attr($atts['small']); ?>,
			        slidesToScroll: 1
			      }
			    },
			    {
			      breakpoint: 480,
			      settings: {
			        slidesToShow: <?php echo esc_attr($atts['xsmall']); ?>,
			        slidesToScroll: 1
			      }
			    }
			]
		});
	});
</script>