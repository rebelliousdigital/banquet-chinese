<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$options = array(
	'img-group' => array(
		'type'    => 'group',
		'options' => array(
			'image' => array(
				'type'   => 'upload',
				'label'  => __( 'Image', 'banquetchinese' ),
				'desc'   => __( 'Upload a featured image.', 'banquetchinese' )
			),
			'pdf'             => array(
				'label'       => __( 'Menu', 'unyson' ),
				'desc'   => __( 'Upload menu.', 'banquetchinese' ),
				'type'        => 'upload',
				'images_only' => false,
			),
		),
	),
	'text-group' => array(
		'type'    => 'group',
		'options' => array(
			'title' => array(
				'label' => __('Title', 'banquetchinese'),
				'desc' => __('Menu title.', 'banquetchinese'),
				'type'  => 'text',
			),
		),
	),
);