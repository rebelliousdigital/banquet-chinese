<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$cfg = array();

$cfg['page_builder'] = array(
	'title'       => __( 'Menu', 'banquetchinese' ),
	'description' => __( 'Add a Menu', 'banquetchinese' ),
	'tab'         => __( 'Content Elements', 'banquetchinese' ),
);
