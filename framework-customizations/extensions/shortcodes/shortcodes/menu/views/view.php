<?php if ( ! defined( 'FW' ) ) { die( 'Forbidden' ); } ?>

<div class="menu scrollme">
	<a href="<?php echo !empty($atts['pdf']['url']) ? esc_url($atts['pdf']['url']) : ''; ?>" title="<?php echo esc_attr($atts['title']); ?>" class="image animateme" data-when="span" data-from="0" data-to="1" data-translatey="50">
		<img src="<?php echo esc_url(fw_resize( $atts['image']['url'], 555, 415, true )); ?>" alt="<?php echo esc_attr($atts['title']); ?>" />
	</a>
	<a href="<?php echo !empty($atts['pdf']['url']) ? esc_url($atts['pdf']['url']) : ''; ?>" title="<?php echo esc_attr($atts['title']); ?>" class="title">
		<h4><?php echo esc_attr($atts['title']); ?></h4>
	</a>
</div>