<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$options = array(
	'links' => array(
		'type' => 'addable-popup',
	    'label' => esc_html__('Links', 'silverbluff'),
	    'desc'  => esc_html__('Add links', 'silverbluff'),
	    'popup-title' => esc_html__('Links','silverbluff'),
	    'limit' => 2,
	    'add-button-text' => esc_html__('Add links', 'silverbluff'),
	    'template' => '{{- btn_text }}',
	    'popup-options' => array(
	    	'btn_text' => array(
				'label' 	=> esc_attr__( 'Button text', 'banquetchinese' ),
				'type'  	=> 'text',
				'value' 	=> '',
				'desc'  	=> false,
			),
			'anchor'  => array(
			    'type'       	=> 'multi-select',
				'label'      	=> esc_attr__( 'Link', 'specto' ),
				'population' 	=> 'posts',
				'source'     	=> 'page',
				'limit' 	 	=> 1,
				'prepopulate' 	=> 999,
				'desc'       	=> esc_attr__('Select a page to link to.', 'specto'),
			),
	    ),
	),
);