<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$cfg = array();

$cfg['page_builder'] = array(
	'title'       => esc_attr__( 'Tab buttons', 'banquetchinese' ),
	'description' => esc_attr__( 'Add a Tab buttons', 'banquetchinese' ),
	'tab'         => esc_attr__( 'Content Elements', 'banquetchinese' ),
);