<?php if (!defined('FW')) die( 'Forbidden' ); ?>

<ul class="tab-btns">
 	<?php foreach ( $atts['links'] as $link ) : ?>

	    <li>
	    	<a href="<?php echo !empty($link['anchor']) ? esc_url(get_the_permalink($link['anchor'][0])) : ''; ?>">
				<?php echo esc_attr($link['btn_text']); ?>
			</a>
	    </li>

    <?php  endforeach; ?>
</ul>