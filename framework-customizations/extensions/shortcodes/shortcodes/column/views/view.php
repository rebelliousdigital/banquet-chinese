<?php

if (!defined('FW')) die('Forbidden');
$class = fw_ext_builder_get_item_width('page-builder', $atts['width'] . '/frontend_class');

?>

<div class="<?php echo esc_attr($class); echo ' '; echo esc_attr($atts['class']); echo ' '; echo esc_attr($atts['align']); echo !empty($atts['background_color']) ? ' shadow' : ''; ?>" style="
	<?php
		echo isset($atts['translatey']) ? 'transform: translateY('. $atts['translatey'] .');' : '';
		echo isset($atts['background_color']) ? ' background-color: '. $atts['background_color'] .';' : '';
	?>">
	<?php if (!empty($atts['background_color'])) { ?>
		<div class="inner-wrap">
	 		<?php echo do_shortcode($content); ?>
	 	</div>
	 <?php } else {
	 	echo do_shortcode($content);
	 } ?>
</div>