<?php if (!defined('FW')) {
	die('Forbidden');
}

$options = array(
	'text-group' => array(
   		'type'    => 'group',
   		'options' => array(
   			'background_color'  => array(
				'label' => esc_attr__( 'Background color', 'banquetchinese' ),
				'type'  => 'color-picker',
				'desc'  => esc_attr__( 'Please select a background color.', 'banquetchinese' ),
			),
   		),
   	),
	'translateY' => array(
		'label' => esc_attr__('TranslateY', 'banquetchinese'),
		'type'  => 'short-text',
	),
	'align' 	=> array(
		'label'   		=> esc_attr__( 'Alignment', 'banquetchinese' ),
		'type'    		=> 'short-select',
		'value'   		=> '',
		'desc'    		=> esc_attr__( 'Align text/content within the column', 'banquetchinese' ),
		'choices' 		=> array(
			'' 	=> esc_attr__('--', 'banquetchinese'),
			'alignleft' 	=> esc_attr__('Left', 'banquetchinese'),
			'aligncenter' 	=> esc_attr__('Center', 'banquetchinese'),
			'alignright' 	=> esc_attr__('Right', 'banquetchinese'),
		),
	),
	'class' => array(
		'label' => esc_attr__('Class', 'banquetchinese'),
		'type'  => 'text',
	),
);
