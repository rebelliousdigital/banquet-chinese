<?php if (!defined('FW')) die( 'Forbidden' );
/**
 * @var $atts
 */

$ID = md5(uniqid(rand(), true));
?>

<header class="title <?php if(($atts['centered'] == 'centered')){ ?>center<?php } ?>" id="header_<?php printf($ID); ?>">

	<?php if ($atts['additional']['choice'] == 'yes' && !empty($atts['additional']['yes']['icon'])) { ?>
		<i class="icon <?php echo esc_attr($atts['additional']['yes']['icon']['icon-class']); ?>"></i>
	<?php } ?>

	<<?php echo esc_attr($atts['heading']); ?> style="color: <?php echo esc_attr($atts['additional']['yes']['title-colour']); ?>;">
		<?php echo esc_attr($atts['title']); ?>
		<span<?php if (isset($atts['additional']['yes'])){ ?> style="color: <?php echo esc_attr($atts['additional']['yes']['subtitle-colour']); ?>;" <?php } ?>><?php echo esc_attr($atts['subtitle']); ?></span>
	</<?php echo esc_attr($atts['heading']); ?>>

</header>