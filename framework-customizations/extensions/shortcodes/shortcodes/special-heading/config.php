<?php if (!defined('FW')) die('Forbidden');

$cfg = array();

$cfg['page_builder'] = array(
	'title'         => esc_attr__('Special Heading', 'banquetchinese'),
	'description'   => esc_attr__('Add a Special Heading', 'banquetchinese'),
	'tab'           => esc_attr__('Content Elements', 'banquetchinese'),
);