<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$options = array(
	'heading-group' => array(
		'type'    => 'group',
		'options' => array(
			'title'    => array(
				'type'  => 'text',
				'label' => esc_attr__( 'Heading Title', 'banquetchinese' ),
				'desc'  => esc_attr__( 'Write the heading title content', 'banquetchinese' ),
			),
			'subtitle'    => array(
				'type'  => 'text',
				'label' => esc_attr__( 'Subtitle', 'banquetchinese' ),
				'desc'  => esc_attr__( 'Write the heading subtitle content', 'banquetchinese' ),
			),
		),
	),
	'heading' => array(
		'type'    => 'select',
		'label'   => esc_attr__('Heading Size', 'banquetchinese'),
		'value'	   => 'h2',
		'choices' => array(
			'h1' => esc_attr__('H1', 'banquetchinese'),
			'h2' => esc_attr__('H2', 'banquetchinese'),
			'h3' => esc_attr__('H3', 'banquetchinese'),
			'h4' => esc_attr__('H4', 'banquetchinese'),
			'h5' => esc_attr__('h5', 'banquetchinese'),
			'h6' => esc_attr__('h6', 'banquetchinese'),
		)
	),
	'centered' => array(
		'type'    => 'switch', 
		'label'   => esc_attr__('Centered', 'banquetchinese'),
		'value'    => 'left', 
		'right-choice' => array(
			'value' => 'centered',
			'label' => esc_attr__('Yes', 'banquetchinese'),
		),
		'left-choice' => array(
			'value' => '',
			'label' => esc_attr__('No', 'banquetchinese'),
		),
	),
	'additional'       => array(
		'type'         => 'multi-picker',
		'label'        => false,
		'desc'         => false,
		'picker'       => array(
			'choice' => array(
				'label'        => esc_attr__( 'Customise', 'banquetchinese' ),
				'type'         => 'switch',
				'value'        => 'no',
				'desc'		   => esc_attr__('Add additional customisation to heading? Colour, Icon etc.', 'banquetchinese'),
				'right-choice' => array(
					'value' => 'yes',
					'label' => esc_attr__( 'Yes', 'banquetchinese' )
				),
				'left-choice'  => array(
					'value' => 'no',
					'label' => esc_attr__( 'No', 'banquetchinese' )
				),
			)
		),
		'choices'      => array(
			'yes' => array(
				'icon'    => array(
					'type'  => 'icon-v2',
					'modal_size' => 'medium',
					'preview_size' => 'large',
					'label' => esc_html__('Choose an Icon', 'silverbluff'),
				),
				'title-colour'  => array(
				    'type'  => 'color-picker',
				    'value' => '',
				    'label' => esc_attr__('Title colour', 'banquetchinese'),
				),
				'subtitle-colour'  => array(
				    'type'  => 'color-picker',
				    'value' => '',
				    'label' => esc_attr__('Subtitle colour', 'banquetchinese'),
				),
			),
		),
		'show_borders' => false,
	),
);