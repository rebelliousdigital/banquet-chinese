<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$options = array(
	'text' => array(
		'type'   => 'wp-editor',
		'label'  => __( 'Content', 'banquetchinese' ),
		'desc'   => __( 'Enter some content for this texblock', 'banquetchinese' )
	),
	'font-group' => array(
		'type'    => 'group',
		'options' => array(
			'font_colour' => array(
				'label' => __('Font colour', 'banquetchinese'),
				'desc'  => __('Select font colour otherwise default colour will be used.', 'banquetchinese'),
				'type'  => 'color-picker',
			),
			'font_size' => array(
				'label' => __('Font size', 'banquetchinese'),
				'desc'  => __('Select font size otherwise default size will be used.', 'banquetchinese'),
				'help'  => __('e.g 25px', 'banquetchinese'),
				'type'  => 'text',
			),
		),
	),
);