<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

/**
 * @var array $atts
 */
?>

<?php if (isset($atts['font_colour']) || $atts['font-size']) { ?>
	<aside style="color: <?php echo wp_kses_post($atts['font_colour']); ?>; font-size: <?php echo wp_kses_post($atts['font_size']); ?>">
		<?php echo do_shortcode( $atts['text'] ); ?>
	</aside>
<?php 
} else {

	echo do_shortcode( $atts['text'] );

} ?>