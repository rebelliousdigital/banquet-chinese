<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$cfg = array();

$cfg['page_builder'] = array(
	'title'       => __( 'Text Block', 'banquetchinese' ),
	'description' => __( 'Add a Text Block', 'banquetchinese' ),
	'tab'         => __( 'Content Elements', 'banquetchinese' ),
);
