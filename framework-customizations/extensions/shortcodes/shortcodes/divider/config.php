<?php if (!defined('FW')) die('Forbidden');

$cfg = array();

$cfg['page_builder'] = array(
	'title'         => esc_attr__('Divider / Spacer', 'banquetchinese'),
	'description'   => esc_attr__('Add a Divider / Space', 'banquetchinese'),
	'tab'           => esc_attr__('Content Elements', 'banquetchinese'),
	'popup_size'    => 'small'
);