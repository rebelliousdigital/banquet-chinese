<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$options = array(
	'style' => array(
		'type'     => 'multi-picker',
		'label'    => false,
		'desc'     => false,
		'picker' => array(
			'ruler_type' => array(
				'type'    => 'select',
				'label'   => esc_attr__( 'Ruler Type', 'banquetchinese' ),
				'value'	   => 'space',
				'desc'    => esc_attr__( 'Here you can set the styling and size of the HR element', 'banquetchinese' ),
				'choices' => array(
					'line'  => esc_attr__( 'Line', 'banquetchinese' ),
					'space' => esc_attr__( 'Whitespace', 'banquetchinese' ),
				)
			)
		),
		'choices'     => array(
			'space' => array(
				'height' => array(
					'label' => esc_attr__( 'Height', 'banquetchinese' ),
					'desc'  => esc_attr__( 'How much whitespace do you need? Enter a pixel value. Positive value will increase the whitespace, negative value will reduce it. eg: \'50\', \'-25\', \'200\'', 'banquetchinese' ),
					'type'  => 'text',
					'value' => '80'
				)
			)
		)
	)
);
