<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$options = array(
	'image'            => array(
		'type'  => 'upload',
		'label' => __( 'Choose Image', 'banquetchinese' ),
		'desc'  => __( 'Either upload a new, or choose an existing image from your media library', 'banquetchinese' )
	),
	'styled'       		=> array(
		'type'         => 'multi-picker',
		'label'        => false,
		'desc'         => false,
		'picker'       => array(
			'options' => array(
				'label'        => esc_attr__( 'Styled?', 'banquetchinese' ),
				'type'         => 'switch',
				'value'        => 'false',
				'desc'         => esc_attr__( 'Styled image with border and shadow? ', 'banquetchinese' ),
				'right-choice' => array(
					'value' => 'true',
					'label' => esc_attr__( 'Yes', 'banquetchinese' )
				),
				'left-choice'  => array(
					'value' => 'false',
					'label' => esc_attr__( 'No', 'banquetchinese' )
				),
			)
		),
	),
	'size'             => array(
		'type'    => 'group',
		'options' => array(
			'width'  => array(
				'type'  => 'text',
				'label' => __( 'Width', 'banquetchinese' ),
				'desc'  => __( 'Set image width', 'banquetchinese' ),
				'value' => 300
			),
			'height' => array(
				'type'  => 'text',
				'label' => __( 'Height', 'banquetchinese' ),
				'desc'  => __( 'Set image height', 'banquetchinese' ),
				'value' => 200
			)
		)
	),
	'image-link-group' => array(
		'type'    => 'group',
		'options' => array(
			'link'   => array(
				'type'  => 'text',
				'label' => __( 'Image Link', 'banquetchinese' ),
				'desc'  => __( 'Where should your image link to?', 'banquetchinese' )
			),
			'target' => array(
				'type'         => 'switch',
				'label'        => __( 'Open Link in New Window', 'banquetchinese' ),
				'desc'         => __( 'Select here if you want to open the linked page in a new window', 'banquetchinese' ),
				'right-choice' => array(
					'value' => '_blank',
					'label' => __( 'Yes', 'banquetchinese' ),
				),
				'left-choice'  => array(
					'value' => '_self',
					'label' => __( 'No', 'banquetchinese' ),
				),
			),
		)
	)
);

