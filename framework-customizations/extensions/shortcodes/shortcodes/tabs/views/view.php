<?php if (!defined('FW')) die( 'Forbidden' ); ?>
<?php $tabs_id = uniqid('fw-tabs-'); ?>
<div class="fw-tabs-container" id="<?php echo esc_attr($tabs_id); ?>">
	<div class="fw-tabs">
		<ul>
			<?php foreach (fw_akg( 'tabs', $atts, array() ) as $key => $tab) : ?>
				<li><a href="#<?php echo esc_attr($tabs_id . '-' . ($key + 1)); ?>"><?php echo $tab['tab_title']; ?></a></li>
			<?php endforeach; ?>
		</ul>
	</div>
	<?php foreach ( $atts['tabs'] as $key => $tab ) : ?>
		<div class="fw-tab-content" id="<?php echo esc_attr($tabs_id . '-' . ($key + 1)); ?>">
			<div class="row">
				<div class="<?php echo $tab['image'] ? 'col-sm-6' : 'col-sm-12'; ?>">
					<h3><?php echo do_shortcode( $tab['title'] ) ?></h3>
					<p><?php echo do_shortcode( $tab['content'] ) ?></p>
				</div>
				<div class="<?php echo $tab['image'] ? 'col-sm-6' : 'hidden-md'; ?>">
					<?php
						if(!empty($tab['image'])){
							echo '<img src="'. $tab['image']['url'] .'" alt="'. $tab['title'] .'" />';
						}
					?>
				</div>
			</div>
		</div>
	<?php endforeach; ?>
</div>