<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$cfg = array();

$cfg['page_builder'] = array(
	'title'       => __( 'Tabs', 'banquetchinese' ),
	'description' => __( 'Add some Tabs', 'banquetchinese' ),
	'tab'         => __( 'Content Elements', 'banquetchinese' ),
);