<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$options = array(
	'tabs' => array(
		'type'          => 'addable-popup',
		'label'         => __( 'Tabs', 'banquetchinese' ),
		'popup-title'   => __( 'Add/Edit Tab', 'banquetchinese' ),
		'desc'          => __( 'Create your tabs', 'banquetchinese' ),
		'template'      => '{{=tab_title}}',
		'popup-options' => array(
			'image' => array(
				'type'  => 'upload',
				'label' => __('Upload', 'banquetchinese')
			),
			'content-group' => array(
				'type'    => 'group',
				'options' => array(
					'tab_title' => array(
						'type'  => 'text',
						'label' => __('Tab title', 'banquetchinese')
					),
					'title' => array(
						'type'  => 'text',
						'label' => __('Title', 'banquetchinese')
					),
					'content' => array(
						'type'  => 'textarea',
						'label' => __('Content', 'banquetchinese')
					),
				),
			),
		),
	)
);