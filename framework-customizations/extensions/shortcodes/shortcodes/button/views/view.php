<?php if (!defined('FW')) die( 'Forbidden' ); ?>

<div class="<?php echo esc_attr($atts['centred']); ?>">
	<a href="<?php echo esc_url(get_the_permalink($atts['anchor'])); ?>" target="<?php echo esc_attr($atts['target']); ?>" class="btn">
		<?php echo esc_attr($atts['btn_text']); ?>
	</a>
</div>