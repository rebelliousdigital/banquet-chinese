<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$options = array(
	'button-group' => array(
		'type'    => 'group',
		'options' => array(
			'btn_text' => array(
				'label' 	=> esc_attr__( 'Button text', 'banquetchinese' ),
				'type'  	=> 'text',
				'value' 	=> '',
				'desc'  	=> false,
			),
		),
	),
	'centred' => array(
		'type'    => 'switch',
		'label'   => esc_attr__('Centred?', 'banquetchinese'),
		'value'        => '',
		'left-choice' => array(
			'value' => 'centred',
			'label' => esc_attr__('Yes', 'banquetchinese'),
		),
		'right-choice' => array(
			'value' => '',
			'label' => esc_attr__('No', 'banquetchinese'),
		),
	),
	'anchor-group' => array(
		'type'    => 'group',
		'options' => array(
			'anchor'  => array(
			    'type'       	=> 'multi-select',
				'label'      	=> esc_attr__( 'Link', 'specto' ),
				'population' 	=> 'posts',
				'source'     	=> 'page',
				'limit' 	 	=> 1,
				'prepopulate' 	=> 999,
				'desc'       	=> esc_attr__('Select a page to link to.', 'specto'),
			),
		    'target' => array(
		        'type'  => 'switch',
				'label'   => esc_attr__( 'Open Link in New Window', 'banquetchinese' ),
				'desc'    => esc_attr__( 'Select here if you want to open the linked page in a new window', 'banquetchinese' ),
		        'right-choice' => array(
		            'value' => '_blank',
		            'label' => esc_attr__('Yes', 'banquetchinese'),
		        ),
		        'left-choice' => array(
		            'value' => '_self',
		            'label' => esc_attr__('No', 'banquetchinese'),
		        ),
		    ),
		),
	),
);