<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

/**
 * @var $map_data_attr
 * @var $atts
 * @var $content
 * @var $tag
 */
if(empty($atts['latitude']) || empty($atts['longitude'])) return;

$colour = ( function_exists( 'fw_get_db_settings_option' ) ) ? fw_get_db_settings_option('primary_colour') : '';
$pin = !empty($atts['pin']) ? $atts['pin']['url'] : get_template_directory_uri() . '/images/pin.svg';
?>
<div id="map" class="<?php echo esc_attr($atts['class']);?>" style="<?php echo esc_attr($atts['map_height']);?>px">
</div>
<script>
	jQuery(document).ready(function($){
		// Google maps
		if ($("#map").length > 0){

			$("#map").gmap3({
				map:{
					options:{
						zoom: <?php echo (int)$atts['zoom'];?>,
						center: new google.maps.LatLng(<?php echo esc_attr($atts['latitude']);?>, <?php echo esc_attr($atts['longitude']);?>),
						mapTypeId: google.maps.MapTypeId.ROADMAP,
						mapTypeControlOptions: {
							mapTypeIds: [google.maps.MapTypeId.ROADMAP, "style1"]
						},
						styles: [
						    {
						        "featureType": "all",
						        "elementType": "labels.text.fill",
						        "stylers": [
						            {
						                "saturation": 36
						            },
						            {
						                "color": "#333333"
						            },
						            {
						                "lightness": 40
						            }
						        ]
						    },
						    {
						        "featureType": "all",
						        "elementType": "labels.text.stroke",
						        "stylers": [
						            {
						                "visibility": "on"
						            },
						            {
						                "color": "#ffffff"
						            },
						            {
						                "lightness": 16
						            }
						        ]
						    },
						    {
						        "featureType": "all",
						        "elementType": "labels.icon",
						        "stylers": [
						            {
						                "visibility": "off"
						            }
						        ]
						    },
						    {
						        "featureType": "administrative",
						        "elementType": "geometry.fill",
						        "stylers": [
						            {
						                "color": "#fefefe"
						            },
						            {
						                "lightness": 20
						            }
						        ]
						    },
						    {
						        "featureType": "administrative",
						        "elementType": "geometry.stroke",
						        "stylers": [
						            {
						                "color": "#fefefe"
						            },
						            {
						                "lightness": 17
						            },
						            {
						                "weight": 1.2
						            }
						        ]
						    },
						    {
						        "featureType": "administrative.country",
						        "elementType": "labels.text.fill",
						        "stylers": [
						            {
						                "color": "#c96055"
						            }
						        ]
						    },
						    {
						        "featureType": "administrative.province",
						        "elementType": "labels.text.fill",
						        "stylers": [
						            {
						                "color": "#c96055"
						            }
						        ]
						    },
						    {
						        "featureType": "administrative.locality",
						        "elementType": "labels.text.fill",
						        "stylers": [
						            {
						                "color": "#6c8c8b"
						            }
						        ]
						    },
						    {
						        "featureType": "administrative.neighborhood",
						        "elementType": "labels.text.fill",
						        "stylers": [
						            {
						                "color": "#cb9954"
						            }
						        ]
						    },
						    {
						        "featureType": "landscape",
						        "elementType": "geometry",
						        "stylers": [
						            {
						                "color": "#f5f5f5"
						            },
						            {
						                "lightness": 20
						            }
						        ]
						    },
						    {
						        "featureType": "poi",
						        "elementType": "geometry",
						        "stylers": [
						            {
						                "color": "#f5f5f5"
						            },
						            {
						                "lightness": 21
						            }
						        ]
						    },
						    {
						        "featureType": "poi.park",
						        "elementType": "geometry",
						        "stylers": [
						            {
						                "color": "#dedede"
						            },
						            {
						                "lightness": 21
						            }
						        ]
						    },
						    {
						        "featureType": "road.highway",
						        "elementType": "geometry.fill",
						        "stylers": [
						            {
						                "color": "#ffffff"
						            },
						            {
						                "lightness": 17
						            }
						        ]
						    },
						    {
						        "featureType": "road.highway",
						        "elementType": "geometry.stroke",
						        "stylers": [
						            {
						                "color": "#ffffff"
						            },
						            {
						                "lightness": 29
						            },
						            {
						                "weight": 0.2
						            }
						        ]
						    },
						    {
						        "featureType": "road.arterial",
						        "elementType": "geometry",
						        "stylers": [
						            {
						                "color": "#ffffff"
						            },
						            {
						                "lightness": 18
						            }
						        ]
						    },
						    {
						        "featureType": "road.local",
						        "elementType": "geometry",
						        "stylers": [
						            {
						                "color": "#ffffff"
						            },
						            {
						                "lightness": 16
						            }
						        ]
						    },
						    {
						        "featureType": "transit",
						        "elementType": "geometry",
						        "stylers": [
						            {
						                "color": "#f2f2f2"
						            },
						            {
						                "lightness": 19
						            }
						        ]
						    },
						    {
						        "featureType": "water",
						        "elementType": "geometry",
						        "stylers": [
						            {
						                "color": "#e9e9e9"
						            },
						            {
						                "lightness": 17
						            }
						        ]
						    }
						],
						streetViewControl: false,
						draggable: true,
						scrollwheel: false
					}
				}
				,
				marker:{
					latLng:[<?php echo esc_attr($atts['latitude']);?>, <?php echo esc_attr($atts['longitude']);?>],
					options:{
						icon: new google.maps.MarkerImage("<?php echo esc_url($pin);?>", new google.maps.Size(<?php echo esc_attr($atts['pin_width']); ?>, <?php echo esc_attr($atts['pin_height']); ?>, "px", "px"))
					}
				}

			});

		}
	});
</script>