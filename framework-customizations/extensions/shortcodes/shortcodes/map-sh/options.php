<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$options = array(
	'latitude'        => array(
		'type'       => 'text',
		'value'      => '',
		'label'      => esc_html__( 'Latitude', 'banquetchinese' ),
		'desc'       => esc_html__( 'Add map latitude', 'banquetchinese' ),
	),

	'longitude'        => array(
		'type'       => 'text',
		'value'      => '',
		'label'      => esc_html__( 'Longitude', 'banquetchinese' ),
		'desc'       => esc_html__( 'Add map longitude', 'banquetchinese' ),
	),
	'gmap-key' => array_merge(
		array(
			'label' => esc_attr__( 'Google Maps API Key', 'banquetchinese' ),
			'desc' => sprintf(
				esc_attr__( 'Create an application in %sGoogle Console%s and add the Key here.', 'banquetchinese' ),
				'<a href="https://console.developers.google.com/flows/enableapi?apiid=places_backend,maps_backend,geocoding_backend,directions_backend,distance_matrix_backend,elevation_backend&keyType=CLIENT_SIDE&reusekey=true">',
				'</a>'
			),
		),
		version_compare(fw()->manifest->get_version(), '2.5.7', '>=')
		? array(
			'type' => 'gmap-key',
		)
		: array(
			'type' => 'text',
			'fw-storage' => array(
				'type'      => 'wp-option',
				'wp_option' => 'fw-option-types:gmap-key',
			),
		)
	),
    'zoom'        => array(
        'type'       => 'slider',
        'value'      => 16,
        'properties' => array(
            'min' => 0,
            'max' => 21,
            'sep' => 1,
        ),
        'label'      => esc_html__( 'Map Zoom', 'banquetchinese' ),
        'desc'       => esc_html__( 'Select map zoom', 'banquetchinese' ),
    ),

    'pin-group' => array(
		'type'    => 'group',
		'options' => array(
		    'pin'        => array(
			    'type'       => 'upload',
			    'value'      => '',
			    'label'      => esc_html__( 'Pin', 'banquetchinese' ),
			    'desc'       => esc_html__( 'Upload map pin', 'banquetchinese' ),
		    ),
		    'pin_height' => array(
				'label' => esc_html__('Pin Height', 'banquetchinese'),
				'desc'  => esc_html__('Set pin height (Ex: 30)', 'banquetchinese'),
				'type'  => 'text',
				'value' => '74'
			),
			'pin_width' => array(
				'label' => esc_html__('Pin Width', 'banquetchinese'),
				'desc'  => esc_html__('Set pin width (Ex: 30)', 'banquetchinese'),
				'type'  => 'text',
				'value' => '60'
			),
		),
	),

	'map_height' => array(
		'label' => esc_html__('Map Height', 'banquetchinese'),
		'desc'  => esc_html__('Set map height (Ex: 300)', 'banquetchinese'),
		'type'  => 'text'
	),

	'class'          => array(
		'type'  => 'text',
		'label' => esc_html__( 'Custom Class', 'banquetchinese' ),
		'desc'  => esc_html__( 'Enter a custom CSS class', 'banquetchinese' ),
		'help'  => esc_html__( 'You can use this class to further style this shortcode by adding your custom CSS', 'banquetchinese' ),
	),
);