<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$options = array(
	'gallery'               => array(
		'label'        => __( 'Gallery', 'unyson' ),
		'type'         => 'addable-box',
		'label'        => __( 'Gallery images', 'unyson' ),
		'box-options'  => array(
			'category'              => array(
				'label'   => __( 'Category', 'unyson' ),
				'type'    => 'short-select',
				'value'   => '1',
				'desc'   => __( 'Select a category for this image.', 'unyson' ),
				'choices' => array(
					'dim-sum' => 'Dim Sum',
					'a-la-carte' => 'À la carte',
					'drinks' => 'Drinks',
					'restaurant' => 'Restaurant',
				),
			),
			'image'     => array(
				'label' => __( 'Image', 'unyson' ),
				'type'  => 'upload',
			),
		),
		'template'     => '{{- category }}',
	),
);