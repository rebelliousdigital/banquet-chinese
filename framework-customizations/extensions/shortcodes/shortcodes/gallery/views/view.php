<?php if ( ! defined( 'FW' ) ) { die( 'Forbidden' ); } ?>
	
<div class="gallery-filter">
    <a href="#" data-filter="all" class="active"><?php echo esc_attr__('All', 'banquetchinese'); ?></a>
    <a href="#" data-filter="dim-sum"><?php echo esc_attr__('Dim Sum', 'banquetchinese'); ?></a>
    <a href="#" data-filter="a-la-carte"><?php echo esc_attr__('À la carte', 'banquetchinese'); ?></a>
    <a href="#" data-filter="drinks"><?php echo esc_attr__('Drinks', 'banquetchinese'); ?></a>
    <a href="#" data-filter="restaurant"><?php echo esc_attr__('Restaurant', 'banquetchinese'); ?></a>
</div>

<div class="row">
	<?php foreach ($atts['gallery'] as $key => $gallery) { ?>
		<div class="gallery col-sm-6 col-md-4 filter <?php echo esc_attr($gallery['category']); ?>">
			<a href="<?php echo esc_url($gallery['image']['url']); ?>" class="gallery" data-gall="photoGallery">
		    	<img src="<?php echo esc_url(fw_resize($gallery['image']['url'], 360, 300, true )); ?>" alt="<?php bloginfo('name'); ?>" />
		    </a>
		</div>
	<?php } ?>
</div>

<script>
jQuery(document).ready(function($){

    $(".gallery-filter a").click(function(){
        var value = $(this).attr('data-filter');
        
        if(value == "all") {
            $('.filter').show('1000');
        } else {
            $(".filter").not('.'+value).hide('3000');
            $('.filter').filter('.'+value).show('3000');
        }

        $(this).addClass("active");
        $(this).siblings().removeClass("active");
        return false;
    });
    

});
</script>