<?php if (!defined('FW')) die('Forbidden');

$cfg = array();

$cfg['page_builder'] = array(
	'title'         => esc_attr__('Gallery', 'banquetchinese'),
	'description'   => esc_attr__('Add a Gallery', 'banquetchinese'),
	'tab'           => esc_attr__('Media Elements', 'banquetchinese'),
	'popup_size'    => 'small'
);