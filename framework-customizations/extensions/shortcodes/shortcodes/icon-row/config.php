<?php if (!defined('FW')) die('Forbidden');

$cfg = array();

$cfg['page_builder'] = array(
	'title'         => __('Icon Row', 'specto'),
	'description'   => __('Add an Icon Row', 'specto'),
	'tab'           => __('Content Elements', 'specto')
);