<?php if ( ! defined( 'FW' ) ) { die( 'Forbidden' ); } ?>


<div class="icon-row scrollme animateme" data-when="span" data-from="1" data-to="0" data-translatey="100">
	<div class="icon">
		<i class="<?php echo esc_attr($atts['icon']['icon-class']); ?>"></i>
	</div>
	<div class="content">
		<?php echo wp_kses_post($atts['content']); ?>
	</div>
</div>