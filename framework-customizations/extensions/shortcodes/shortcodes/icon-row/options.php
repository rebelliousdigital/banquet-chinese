<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$options = array(
	'icon'    => array(
		'type'  => 'icon-v2',
		'label' => esc_attr__('Choose an Icon', 'banquetchinese'),
	),
	'content' => array(
		'type'  => 'wp-editor',
		'label' => esc_attr__( 'Content', 'banquetchinese' )
	),
);