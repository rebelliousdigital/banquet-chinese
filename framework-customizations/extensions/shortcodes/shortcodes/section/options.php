<?php if (!defined('FW')) {
	die('Forbidden');
}

$options = array(
	'is_fullwidth' => array(
		'label'        => esc_attr__('Full Width', 'banquetchinese'),
		'type'         => 'switch',
	),
	'background_color' => array(
		'label' => esc_attr__('Background Color', 'specto'),
		'desc'  => esc_attr__('Please select the background color', 'specto'),
		'type'  => 'color-picker',
	),
	'background_image' => array(
		'label'   => esc_attr__('Background Image', 'banquetchinese'),
		'desc'    => esc_attr__('Please select the background image', 'banquetchinese'),
		'type'    => 'background-image'
	),
	'video' => array(
		'label' => esc_attr__('Background Video', 'banquetchinese'),
		'desc'  => esc_attr__('Insert Video URL to embed this video', 'banquetchinese'),
		'type'  => 'text',
	),
	'padding-group' => array(
		'type'    => 'group',
		'options' => array(
			'padding_top' => array(
				'label' => esc_attr__('Padding top', 'banquetchinese'),
				'value' => '85px',
				'help'  => esc_attr__('e.g 80px', 'banquetchinese'),
				'type'  => 'text',
			),
			'padding_bottom' => array(
				'label' => esc_attr__('Padding bottom', 'banquetchinese'),
				'value' => '85px',
				'help'  => esc_attr__('e.g 80px', 'banquetchinese'),
				'type'  => 'text',
			),
		),
	),
	'anchorID' => array(
		'label' => esc_attr__('ID', 'banquetchinese'),
		'type'  => 'short-text',
	),
);
