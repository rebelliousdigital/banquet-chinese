<?php if (!defined('FW')) die('Forbidden');

$cfg = array(
	'page_builder' => array(
		'tab'         => esc_attr__('Layout Elements', 'banquetchinese'),
		'title'       => esc_attr__('Section', 'banquetchinese'),
		'description' => esc_attr__('Add a Section', 'banquetchinese'),
		'type'        => 'section', // WARNING: Do not edit this
	)
);