<?php
/**
 * The Header for our theme
 *
 * Displays all of the <head> section and everything up till <div id="main">
 */
$page = ( function_exists( 'fw_get_db_post_option' ) ) ? fw_get_db_post_option() : '';
?>

<!DOCTYPE html>
<html <?php language_attributes(); ?>>
  <head>
  <meta charset="<?php bloginfo( 'charset' ); ?>">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
  <?php wp_head(); ?>
  </head>

<body <?php body_class(); echo isset($page['headerColour']) ? 'id="'. esc_attr($page['headerColour']) .'"' : ''; ?>>

<div class="wrapper">