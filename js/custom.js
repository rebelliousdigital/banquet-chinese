(function($) {
	"use strict"

	// Headhesive
	var options = {
	    offset: 400,
	    classes: {
	        clone:   'banner--clone',
	        stick:   'banner--stick',
	        unstick: 'banner--unstick'
	    }
	};

	// Initialise with options
	var banner = new Headhesive('.navbar', options);

	// Back to top button
	if ($('#back-to-top').length) {
	    var scrollTrigger = 500, // px
	        backToTop = function () {
	            var scrollTop = $(window).scrollTop();
	            if (scrollTop > scrollTrigger) {
	                $('#back-to-top').addClass('show');
	            } else {
	                $('#back-to-top').removeClass('show');
	            }
	        };
	    backToTop();
	    $(window).on('scroll', function () {
	        backToTop();
	    });
	    $('#back-to-top').on('click', function (e) {
	        e.preventDefault();
	        $('html,body').animate({
	            scrollTop: 0
	        }, 700);
	    });
	}

    $('#back-to-top').click(function () {
        $("body").animate({
            scrollTop: 0
        }, 600);
        return false;
    });

	// Toggle class for burger menu
    $('.navbar-toggle').on('click', function(event) {
    	$(this).toggleClass('minimize');
    	$('body').toggleClass('open');
    });

    // Gallery modal
    $('a.gallery').venobox({
    	overlayColor: 'rgba(255, 255, 255, .7)',
    	titleattr: 'data-title'
    });

    // Custom select
    $('select').wrap('<div class="select-wrapper"></div>');
    

})(jQuery);	