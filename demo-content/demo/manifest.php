<?php if (!defined('FW')) die('Forbidden');
/**
 * @var string $uri Demo directory url
 */

$manifest = array();
$manifest['title'] = esc_attr__('Banquet Chinese', 'banquetchinese');
$manifest['screenshot'] = $uri . '/screenshot.png';
$manifest['preview_link'] = 'http://www.klevermedia.co.uk';