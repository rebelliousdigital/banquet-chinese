<div class="post-section section <?php if ($wp_query->current_post % 2 == 0) { echo 'even'; } else { echo 'odd'; } ?>">

	<div class="container">
		<div class="content-block scrollme">
			<div class="image animateme" data-when="span" data-from="0" data-to="1" data-translatey="50">
				<a href="<?php the_permalink(); ?>" style="background-image: url(<?php the_post_thumbnail_url($size = 'full'); ?>);">
				</a>
			</div>
			<div class="content animateme" data-when="span" data-from="1" data-to="0" data-translatey="50">
				<p class="time"><?php the_time('d F, Y'); ?></p>
				<h2><?php the_title(); ?></h2>
				
				<?php the_excerpt(); ?>
				
				<p>
					<a href="<?php the_permalink(); ?>" class="btn">
						<?php echo esc_attr__('Read more', 'banquetchinese'); ?>
					</a>
				</p>
			</div>
		</div>
	</div>

</div>