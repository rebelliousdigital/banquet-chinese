<?php
/**
 * The Template for displaying all single posts
 */

get_header(); 

?>

<div class="static-header green">
	<?php get_template_part('inc/header', 'nav'); ?>
</div>

<div class="page-title">
	<h1><?php single_cat_title(); ?></h1>
	<?php
		if(function_exists('fw_ext_breadcrumbs')) {
			fw_ext_breadcrumbs('&middot;');
			}
		?>
</div>

<div class="container section">
	<div class="row new-post">

		<?php
		
		$args = array(
			'posts_per_page' => 1
		);

		$query = new WP_Query( $args );
		while ( $query->have_posts() ) : $query->the_post(); ?>
		
		<div class="col-sm-6 img" style="background-image: url(<?php echo get_the_post_thumbnail_url('', 'full'); ?>)">
			<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"></a>
			<span class="date">
				<?php echo esc_html(get_the_date('j')); ?>
				<i><?php echo esc_html(get_the_date('M')); ?></i>
			</span>
		</div>
		<div class="col-sm-6 content">
			<h3><?php the_title(); ?></h3>			
			<?php the_excerpt(); ?>
			<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>" class="arrow-link">
				<i class="fa fa fa-long-arrow-right before"></i>
				<?php echo esc_attr__('Read full post', 'banquetchinese'); ?>
				<i class="fa fa fa-long-arrow-right after"></i>
			</a>
		</div>

		<?php endwhile; wp_reset_postdata(); ?>

	</div>
</div>

<div class="container section">
	<div class="row">

		<div class="col-sm-8 col-md-9">

			<?php
			
			$args = array(
				'offset'		 => 1
			);

			$query = new WP_Query( $args );
			while ( $query->have_posts() ) : $query->the_post();
			
			// Include the page content template.
			get_template_part( 'excerpt', 'page' );
			
			// If comments are open or we have at least one comment, load up the comment template.
			if ( comments_open() || get_comments_number() ) {
				comments_template();
			}

			endwhile; wp_reset_postdata(); ?>
		</div>
		<div class="col-sm-4 col-md-3" id="sidebar">
			<?php dynamic_sidebar('sidebar-blog'); ?>
		</div>

	</div>
</div>

<div class="container-fluid section page-wrap">
	<div class="row">
		<div class="col-sm-12">
			<?php banquet_chinese_paging_nav(); ?>
		</div>
	</div>
</div>

<?php get_footer(); ?>