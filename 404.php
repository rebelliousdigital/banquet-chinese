<?php
/**
 * The template for displaying 404 pages (Not Found)
 */

get_header();
$page = ( function_exists( 'fw_get_db_settings_option' ) ) ? fw_get_db_settings_option() : '';
$title = ( function_exists( 'fw_get_db_settings_option' ) ) ? fw_get_db_settings_option('errorTitle') : '';
$subtitle = ( function_exists( 'fw_get_db_settings_option' ) ) ? fw_get_db_settings_option('errorSubTitle') : '';
$content = ( function_exists( 'fw_get_db_settings_option' ) ) ? fw_get_db_settings_option('errorContent') : '';
$search = ( function_exists( 'fw_get_db_settings_option' ) ) ? fw_get_db_settings_option('errorSearch') : '';
?>

<div class="static-header blue">
	<?php get_template_part('inc/header', 'nav'); ?>
</div>

<div class="page-title">
	<h1><?php echo !empty($page['pageTitle']) ? $page['pageTitle'] : the_title(); ?></h1>
	<?php
		if(function_exists('fw_ext_breadcrumbs')) {
			fw_ext_breadcrumbs('&middot;');
			}
			?>
</div>



<div class="container centred error-404">
	<div class="row">
		<div class="col-sm-12">
			
			<h2>
				<?php echo esc_html__('Sorry, we\'re a litle lost&hellip;', 'banquetchinese'); ?>
			</h2>

			<p><?php echo esc_html__('It looks like nothing was found at this location. Maybe try a search?', 'banquetchinese'); ?></p>

			<a href="<?php echo esc_url( apply_filters( 'woocommerce_return_to_shop_redirect', wc_get_page_permalink( 'shop' ) ) ); ?>" class="btn blue">
				<?php echo esc_attr__( 'Return to shop', 'banquetchinese' ); ?>
			</a>
		</div>
	</div>
</div>


<?php
get_footer();